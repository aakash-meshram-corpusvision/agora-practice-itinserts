import React, {useState} from 'react'
import AgoraRTC, {IAgoraRTCClient} from 'agora-rtc-sdk-ng'
import useStreaming from '../../hooks/useStreaming'
import MediaPlayer from '../../components/mediaPlayer'
import styles from './ongoing.module.css'

/* export const getStaticPaths = () => ({
  paths: [
    {params: {function: 'voice'}},
    {params: {function: 'video'}},
    {params: {function: 'streaming'}},
  ],
  fallback: false,
})

export const getStaticProps = ({params}) => ({
  props: {function: params.function},
}) */

const client: IAgoraRTCClient = AgoraRTC.createClient({
  codec: 'h264',
  mode: 'live',
})

function Call() {
  const [appid, setAppid] = useState('fdd0d0b23624424ab2271994624c7b1e')
  const [token, setToken] = useState(
    '006fdd0d0b23624424ab2271994624c7b1eIAD+W23LOS6EyAO0u1h9SAJunEKNAgRN8DDCQoOi97UtFkNmo3UAAAAAEACpE93Igk/9XwEAAQCBT/1f'
    /* '006fdd0d0b23624424ab2271994624c7b1eIABXTtp6OtTUIXotey4/eJyrH0EGTeCVBgQeq77WqlQdokNmo3UAAAAAEACZqwdII3r5XwEAAQAjevlf' */
  )
  const [channel, setChannel] = useState('itinserts')
  const uuid = `${Math.random()}`.slice(2)
  const {
    localAudioTrack,
    localVideoTrack,
    leave,
    join,
    joinState,
    remoteUsers,
    toggleMic,
    isMicMuted,
    toggleVid,
    isVidMuted,
    isHost,
    remoteAudioTrack,
    remoteVideoTrack,
  } = useStreaming(client)

  return (
    <div className={styles.call}>
      <form className={styles.callForm}>
        <label>
          AppID:
          <input
            type="text"
            name="appid"
            onChange={event => {
              setAppid(event.target.value)
            }}
          />
        </label>
        <label>
          Token(Optional):
          <input
            type="text"
            name="token"
            onChange={event => {
              setToken(event.target.value)
            }}
            className={styles.Input}
          />
        </label>
        <label>
          Channel:
          <input
            type="text"
            name="channel"
            onChange={event => {
              setChannel(event.target.value)
            }}
            className={styles.Input}
          />
        </label>
        <div className={styles.buttonGroup}>
          <button
            id="join"
            type="button"
            disabled={joinState}
            onClick={() => {
              join(
                'host',
                appid,
                channel,
                token /* , `${Math.random()}`.slice(2) */
              )
            }}
            className={styles.Button}
          >
            Join As a Host
          </button>
          <button
            id="join"
            type="button"
            disabled={joinState}
            onClick={() => {
              join(
                'audience',
                appid,
                channel,
                token /* ,
                `${Math.random()}`.slice(2) */
              )
            }}
            className={styles.Button}
          >
            Join As Audience
          </button>
          <button
            id="leave"
            type="button"
            disabled={!joinState}
            onClick={() => {
              leave()
            }}
            className={styles.Button}
          >
            Leave
          </button>
        </div>
      </form>
      <div className={styles.Screen}>
        <div className={styles.Host}>
          {isHost ? (
            <MediaPlayer
              videoTrack={localVideoTrack}
              audioTrack={localAudioTrack}
              style={{width: '640px', height: '480px'}}
            ></MediaPlayer>
          ) : (
            <MediaPlayer
              videoTrack={remoteVideoTrack}
              audioTrack={remoteAudioTrack}
              style={{width: '640px', height: '480px'}}
            ></MediaPlayer>
          )}
        </div>
      </div>
    </div>
  )
}

export default Call

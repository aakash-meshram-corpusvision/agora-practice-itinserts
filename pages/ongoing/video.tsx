import React, {useState} from 'react'
import AgoraRTC, {IAgoraRTCClient} from 'agora-rtc-sdk-ng'
import useVideo from '../../hooks/useVideo'
import MediaPlayer from '../../components/mediaPlayer'
import styles from './ongoing.module.css'

/* export const getStaticPaths = () => ({
  paths: [
    {params: {function: 'voice'}},
    {params: {function: 'video'}},
    {params: {function: 'streaming'}},
  ],
  fallback: false,
})

export const getStaticProps = ({params}) => ({
  props: {function: params.function},
}) */

const client: IAgoraRTCClient = AgoraRTC.createClient({
  codec: 'h264',
  mode: 'rtc',
})

function Call() {
  const [appid, setAppid] = useState('fdd0d0b23624424ab2271994624c7b1e')
  const [token, setToken] = useState(
    '006fdd0d0b23624424ab2271994624c7b1eIAD+W23LOS6EyAO0u1h9SAJunEKNAgRN8DDCQoOi97UtFkNmo3UAAAAAEACpE93Igk/9XwEAAQCBT/1f'
    /* '006fdd0d0b23624424ab2271994624c7b1eIABXTtp6OtTUIXotey4/eJyrH0EGTeCVBgQeq77WqlQdokNmo3UAAAAAEACZqwdII3r5XwEAAQAjevlf' */
  )
  const [channel, setChannel] = useState('itinserts')
  const {
    localAudioTrack,
    localVideoTrack,
    leave,
    join,
    joinState,
    remoteUsers,
  } = useVideo(client)

  return (
    <div className={styles.call}>
      <form className={styles.callForm}>
        <label>
          AppID:
          <input
            type="text"
            name="appid"
            onChange={event => {
              setAppid(event.target.value)
            }}
          />
        </label>
        <label>
          Token(Optional):
          <input
            type="text"
            name="token"
            onChange={event => {
              setToken(event.target.value)
            }}
            className={styles.Input}
          />
        </label>
        <label>
          Channel:
          <input
            type="text"
            name="channel"
            onChange={event => {
              setChannel(event.target.value)
            }}
            className={styles.Input}
          />
        </label>
        <div className={styles.buttonGroup}>
          <button
            id="join"
            type="button"
            disabled={joinState}
            onClick={() => {
              join(appid, channel, token, 'aakash')
            }}
            className={styles.Button}
          >
            Join
          </button>
          <button
            id="leave"
            type="button"
            // className="btn btn-primary btn-sm"
            disabled={!joinState}
            onClick={() => {
              leave()
            }}
            className={styles.Button}
          >
            Leave
          </button>
        </div>
      </form>
      <div className={styles.Tiles}>
        <div className={styles.Tile}>
          <p /* className="local-player-text" */>
            {localVideoTrack && `localTrack`}
            {joinState && localVideoTrack ? `(${client.uid})` : ''}
          </p>
          <MediaPlayer
            videoTrack={localVideoTrack}
            audioTrack={localAudioTrack}
          ></MediaPlayer>
        </div>
        {remoteUsers.map(user => (
          <div className={styles.Tile} key={user.uid}>
            <p /* className="remote-player-text" */
            >{`remoteVideo(${user.uid})`}</p>
            <MediaPlayer
              videoTrack={user.videoTrack}
              audioTrack={user.audioTrack}
            ></MediaPlayer>
          </div>
        ))}
      </div>
    </div>
  )
}

export default Call

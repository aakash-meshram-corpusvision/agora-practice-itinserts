import Head from 'next/head'
import styles from '../styles/Home.module.css'
import Layout from '../components/layout'
import Home from '../components/home'

export default function Main() {
  return (
    <div className={styles.container}>
      <Head>
        <title>Agora Calling</title>
        <link rel="icon" href="/008-global.png" />
      </Head>
      <Layout>
        <Home />
      </Layout>
    </div>
  )
}

{
  /* <footer className={styles.footer}>
        <a
          href="https://vercel.com?utm_source=create-next-app&utm_medium=default-template&utm_campaign=create-next-app"
          target="_blank"
          rel="noopener noreferrer"
        >
          Powered by{' '}
          <img src="/vercel.svg" alt="Vercel Logo" className={styles.logo} />
        </a>
      </footer> */
}

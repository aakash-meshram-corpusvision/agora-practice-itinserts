import React, {useState, useCallback} from 'react'
import styles from './login.module.css'
import Link from 'next/link'

export const getStaticPaths = () => ({
  paths: [
    {params: {function: 'voice'}},
    {params: {function: 'video'}},
    {params: {function: 'streaming'}},
  ],
  fallback: false,
})

export const getStaticProps = ({params}) => ({
  props: {function: params.function},
})

const Login = props => {
  const [username, setUsername] = useState('')
  const [password, setPassword] = useState('')
  const [error, setError] = useState(null)

  const onSubmitHandler = useCallback(
    e => {
      e.preventDefault()
      if (username.length >= 6) {
        if (username !== password) {
          setError('Incorrect password or username. Please try again.')
        } else {
          setError(null)
          document.getElementById('uwu').click()
        }
      } else {
        setError('Username should be more than or equal to 6 characters')
      }
    },
    [username, password]
  )

  return (
    <div className={styles.Login}>
      <p style={{color: 'grey'}}>The username and password should match</p>
      <p className={styles.Error}>{error}</p>
      <p>Username:</p>
      <input type="text" onChange={e => setUsername(e.target.value)} />
      <p>Password:</p>
      <input type="password" onChange={e => setPassword(e.target.value)} />
      <button type="submit" onClick={onSubmitHandler}>
        Submit
      </button>
      <Link href={`/ongoing/${props.function}`}>
        <div id="uwu"></div>
      </Link>
    </div>
  )
}

export default Login

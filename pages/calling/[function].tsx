import React from 'react'
import styles from './calling.module.css'

export const getStaticPaths = () => ({
  paths: [
    {params: {function: 'voice'}},
    {params: {function: 'video'}},
    {params: {function: 'streaming'}},
  ],
  fallback: false,
})

export const getStaticProps = ({params}) => ({
  props: {function: params.function},
})

const Calling = () => {
  return (
    <div className={styles.Calling}>
      <button>Generate a call</button>
      <p>Enter a Token</p>
      <input type="text" />
      <button>Enter</button>
    </div>
  )
}

export default Calling

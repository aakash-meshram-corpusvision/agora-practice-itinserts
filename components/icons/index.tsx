import React from 'react'
import styles from './style.module.css'

interface props {
  src: string
}

const Icon: React.FC<props> = ({src}) => {
  return (
    <div className={styles.icon}>
      <img src={src} className={styles.image} />
    </div>
  )
}

export default Icon

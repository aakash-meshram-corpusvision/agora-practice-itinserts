import React from 'react'
import styles from './style.module.css'

const Layout = ({children}) => {
  return (
    <div className={styles.Layout}>
      <header></header>
      <main className={styles.Main}>{children}</main>
    </div>
  )
}

export default Layout

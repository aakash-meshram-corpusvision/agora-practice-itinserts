import React from 'react'
import Icon from '../icons'
import styles from './style.module.css'
import Link from 'next/link'

const Home = () => {
  return (
    <div className={styles.Home}>
      <Link href="/login/voice">
        <div>
          <Icon src="/images/icons/svg/007-phone call.svg" />
          <p>Voice calling</p>
        </div>
      </Link>
      <Link href="/login/video">
        <div>
          <Icon src="/images/icons/svg/031-news.svg" />
          <p>Video calling</p>
        </div>
      </Link>
      <Link href="/login/streaming">
        <div>
          <Icon src="/images/icons/svg/046-target.svg" />
          <p>Streaming</p>
        </div>
      </Link>
    </div>
  )
}

export default Home

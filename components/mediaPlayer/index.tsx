import {
  ILocalVideoTrack,
  IRemoteVideoTrack,
  ILocalAudioTrack,
  IRemoteAudioTrack,
} from 'agora-rtc-sdk-ng'
import React, {useRef, useEffect} from 'react'

export interface VideoPlayerProps {
  videoTrack: ILocalVideoTrack | IRemoteVideoTrack | undefined
  audioTrack: ILocalAudioTrack | IRemoteAudioTrack | undefined
  style?: Object
}

const MediaPlayer = (props: VideoPlayerProps) => {
  const container = useRef<HTMLDivElement>(null)

  useEffect(() => {
    console.log('vt', props.videoTrack)
    console.log('c', container)
    if (!container.current) return
    props.videoTrack?.play(container.current)
    return () => {
      props.videoTrack?.stop()
    }
  }, [container, props.videoTrack])

  useEffect(() => {
    console.log('at', props.audioTrack)
    props.audioTrack?.play()
    return () => {
      props.audioTrack?.stop()
    }
  }, [props.audioTrack])

  return (
    <div
      ref={container}
      className="video-player"
      style={{width: '320px', height: '240px', ...props.style}}
    ></div>
  )
}

export default MediaPlayer

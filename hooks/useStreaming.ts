import {useState, useEffect, useRef} from 'react'
import AgoraRTC, {
  IAgoraRTCClient,
  IAgoraRTCRemoteUser,
  MicrophoneAudioTrackInitConfig,
  CameraVideoTrackInitConfig,
  IMicrophoneAudioTrack,
  ICameraVideoTrack,
  ILocalVideoTrack,
  ILocalAudioTrack,
  IRemoteVideoTrack,
  IRemoteAudioTrack,
} from 'agora-rtc-sdk-ng'

export default function useAgoraStreaming(
  streamingClient: IAgoraRTCClient | undefined
): {
  localAudioTrack: ILocalAudioTrack | undefined
  localVideoTrack: ILocalVideoTrack | undefined
  joinState: boolean
  leave: Function
  join: Function
  toggleMic: Function
  toggleVid: Function
  isMicMuted: boolean
  isVidMuted: boolean
  remoteUsers: IAgoraRTCRemoteUser[]
  isHost: boolean
  remoteVideoTrack: IRemoteVideoTrack | undefined
  remoteAudioTrack: IRemoteAudioTrack | undefined
} {
  const [localVideoTrack, setLocalVideoTrack] = useState<
    ILocalVideoTrack | undefined
  >(undefined)
  const [localAudioTrack, setLocalAudioTrack] = useState<
    ILocalAudioTrack | undefined
  >(undefined)
  const [remoteAudioTrack, setRemoteAudioTrack] = useState<
    IRemoteAudioTrack | undefined
  >(undefined)
  const [remoteVideoTrack, setRemoteVideoTrack] = useState<
    IRemoteVideoTrack | undefined
  >(undefined)

  const [joinState, setJoinState] = useState(false)
  const [isHost, setIsHost] = useState(false)
  const [isMicMuted, setIsMicMuted] = useState(false)
  const [isVidMuted, setIsVidMuted] = useState(false)

  const [remoteUsers, setRemoteUsers] = useState<IAgoraRTCRemoteUser[]>([])

  async function createLocalTracks(
    audioConfig?: MicrophoneAudioTrackInitConfig,
    videoConfig?: CameraVideoTrackInitConfig
  ): Promise<[IMicrophoneAudioTrack, ICameraVideoTrack]> {
    const [
      microphoneTrack,
      cameraTrack,
    ] = await AgoraRTC.createMicrophoneAndCameraTracks(audioConfig, videoConfig)
    setLocalAudioTrack(microphoneTrack)
    setLocalVideoTrack(cameraTrack)
    return [microphoneTrack, cameraTrack]
  }

  async function join(
    role: any,
    appid: string,
    channel: string,
    token?: string,
    uid?: string | number | null
  ) {
    if (!streamingClient) return
    streamingClient.setClientRole(role)
    if (role == 'host') {
      setIsHost(true)
      const [microphoneTrack, cameraTrack] = await createLocalTracks()

      await streamingClient.join(appid, channel, token || null, uid || null)
      await streamingClient.publish([microphoneTrack, cameraTrack])
      ;(window as any).streamingClient = streamingClient
      ;(window as any).videoTrack = cameraTrack
    } else {
      setIsHost(false)
      await streamingClient.join(appid, channel, token || null, uid || null)
      ;(window as any).streamingClient = streamingClient
    }

    setJoinState(true)
  }

  async function toggleMic() {
    if (!isMicMuted) {
      console.log('Mic Muted')
      if (localAudioTrack) {
        localAudioTrack.stop()
        setIsMicMuted(true)
      }
    } else {
      console.log('Mic Unmuted')
      if (localAudioTrack) {
        localAudioTrack.play()
        setIsMicMuted(false)
      }
    }
  }

  async function toggleVid() {
    if (!isVidMuted) {
      console.log('vid off')
      if (localVideoTrack) {
        localVideoTrack.stop()
        setIsVidMuted(true)
      }
    } else {
      console.log('vid on')
      if (localVideoTrack) {
        // localVideoTrack.play()
        setIsVidMuted(false)
      }
    }
  }

  async function leave() {
    if (localAudioTrack) {
      localAudioTrack.stop()
      localAudioTrack.close()
    }
    if (localVideoTrack) {
      localVideoTrack.stop()
      localVideoTrack.close()
    }
    setRemoteUsers([])
    setJoinState(false)
    await streamingClient?.leave()
    // window.history.back()
  }

  useEffect(() => {
    if (!streamingClient) return
    setRemoteUsers(streamingClient.remoteUsers)

    const handleUserPublished = async (
      user: IAgoraRTCRemoteUser,
      mediaType: 'audio' | 'video'
    ) => {
      await streamingClient.subscribe(user, mediaType)
      setRemoteUsers(remoteUsers => Array.from(streamingClient.remoteUsers))
      if (mediaType == 'video') {
        setRemoteVideoTrack(user.videoTrack)
      }
      if (mediaType == 'audio') {
        setRemoteAudioTrack(user.audioTrack)
      }
    }
    const handleUserUnpublished = (user: IAgoraRTCRemoteUser) => {
      setRemoteUsers(remoteUsers => Array.from(streamingClient.remoteUsers))
    }
    const handleUserJoined = (user: IAgoraRTCRemoteUser) => {
      setRemoteUsers(remoteUsers => Array.from(streamingClient.remoteUsers))
    }
    const handleUserLeft = (user: IAgoraRTCRemoteUser) => {
      setRemoteUsers(remoteUsers => Array.from(streamingClient.remoteUsers))
    }
    streamingClient.on('user-published', handleUserPublished)
    streamingClient.on('user-unpublished', handleUserUnpublished)
    streamingClient.on('user-joined', handleUserJoined)
    streamingClient.on('user-left', handleUserLeft)

    return () => {
      streamingClient.off('user-published', handleUserPublished)
      streamingClient.off('user-unpublished', handleUserUnpublished)
      streamingClient.off('user-joined', handleUserJoined)
      streamingClient.off('user-left', handleUserLeft)
    }
  }, [streamingClient])

  return {
    localAudioTrack,
    localVideoTrack,
    joinState,
    leave,
    join,
    toggleMic,
    toggleVid,
    remoteUsers,
    isMicMuted,
    isVidMuted,
    isHost,
    remoteVideoTrack,
    remoteAudioTrack,
  }
}
